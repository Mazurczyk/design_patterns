package command;

public class ChangeSourceTVCommand implements Command {

    private TV tv;
    private SourceType sourceType;

    public ChangeSourceTVCommand(TV tv, SourceType sourceType) {
        this.tv = tv;
        this.sourceType = sourceType;
    }

    @Override
    public void execute() {
        tv.setSourceType(sourceType);
        System.out.println("TV source type is: " + tv.getSourceType());
    }
}
