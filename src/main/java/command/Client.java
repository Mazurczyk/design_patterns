package command;

//1. Napisz aplikację realizującą wzorzec projektowy polecenie na przykładzie Telewizora.
//​
//Telewizor powinien móc wspierać:
//- włączanie
//- wyłączanie
//- zmianę źródła (HDMI1, HDMI2)
//- zmianę poziomu głośności
//​
//Na potrzeby zadania stwórz klasę:
//- TV (odpowiednik klasy Light z przykładu)
//- Command (abstrakcja i konkretne polecenia)
//- RemoteController - obłsuga poleceń

public class Client {
    public static void main(String[] args) {
        TV tv = new TV();
        Command turnOn = new TurnOnTVCommand(tv);
        Command turnOff = new TurnOffTVCommand(tv);
        Command changeSource = new ChangeSourceTVCommand(tv, SourceType.HDMI1);
        Command increaseSoundLevel = new IncreaseSoundLevelTVCommand(tv);
        Command decreaseSoundLevel = new DecreaseSoundLevelTVCommand(tv);
        RemoteController remoteController = new RemoteController();

        remoteController.setCommand(turnOn);
        remoteController.execute();

        remoteController.setCommand(turnOff);
        remoteController.execute();

        remoteController.setCommand(changeSource);
        remoteController.execute();

        remoteController.setCommand(increaseSoundLevel);
        remoteController.execute();

        remoteController.setCommand(decreaseSoundLevel);
        remoteController.execute();

    }
}
