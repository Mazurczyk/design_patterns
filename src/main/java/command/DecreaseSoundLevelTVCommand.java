package command;

public class DecreaseSoundLevelTVCommand implements Command {

    private TV tv;

    public DecreaseSoundLevelTVCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        if (tv.getSoundLevel() > TV.MIN_SOUND_LEVEL) tv.decreaseVolume();
        System.out.println("TV sound level is: " + tv.getSoundLevel());
    }
}
