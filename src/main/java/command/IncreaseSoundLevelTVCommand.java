package command;

public class IncreaseSoundLevelTVCommand implements Command {

    private TV tv;

    public IncreaseSoundLevelTVCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        if (tv.getSoundLevel() < TV.MAX_SOUND_LEVEL) tv.increaseVolume();
        System.out.println("TV sound level is: " + tv.getSoundLevel());
    }
}
