package command;


public class TV {
    final static Integer MAX_SOUND_LEVEL = 100;
    final static Integer MIN_SOUND_LEVEL = 0;

    private boolean isOn;
    private SourceType sourceType;
    private Integer soundLevel;

    public TV() {
        isOn = false;
        sourceType = SourceType.TV;
        soundLevel = MIN_SOUND_LEVEL;
    }

    public void turnOn(){
        isOn = true;
    }

    public void turnOff(){
        isOn = false;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public void increaseVolume(){
        soundLevel++;
    }

    public void decreaseVolume(){
        soundLevel--;
    }

    public Integer getSoundLevel() {
        return soundLevel;
    }

    public boolean isOn() {
        return isOn;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

}
