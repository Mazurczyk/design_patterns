package command;

public class TurnOffTVCommand implements Command {

    private TV tv;

    public TurnOffTVCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.turnOn();
        System.out.println("TV is turned on: " + tv.isOn());
    }
}
