package fasada;

public class Main {
    public static void main(String[] args) {
        ShopAPI shopAPI = new ShopAPI();
        shopAPI.register("rafikic","sialala");
        shopAPI.login("rafikic","sialala");
        shopAPI.feedWallet(1000);
        shopAPI.addProduct("patelnia",100);
        shopAPI.buyProduct("patelnia");
        shopAPI.removeProduct("patelnia");
        shopAPI.logout();
    }
}
