package fasada;

public class PaymentAPI {
    void pay(String productName, ProductAPI productAPI, UserAPI userAPI) {
        Product product = productAPI.getProductByName(productName);
        User user = userAPI.getActiveUser();
        float userWallet = user.getWallet();
        if (product.getPrice() <= userWallet) {
            System.out.println("Product " + product.getName() + " has been bought for " + product.getPrice() + " USD.");
            user.setWallet(userWallet - product.getPrice());
        } else {
            System.out.println("You must feed your wallet!");
        }
    }
}
