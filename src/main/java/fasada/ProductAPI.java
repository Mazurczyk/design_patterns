package fasada;

import java.util.ArrayList;
import java.util.List;

public class ProductAPI {
    private List<Product> productList;

    public ProductAPI() {
        productList = new ArrayList<>();
    }

    public Product getProductByName(String name) {
        for (Product p : productList) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void addProduct(String productName, float price) {
        Product product = new Product(productName, price);
        productList.add(product);
    }

    public void removeProduct(String productName) {
        Product product = getProductByName(productName);
        if (product != null) productList.remove(product);
        else System.out.println("Product is not exist!");
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
