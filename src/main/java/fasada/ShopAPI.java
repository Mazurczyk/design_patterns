package fasada;

public class ShopAPI {

    private PaymentAPI paymentAPI;
    private ProductAPI productAPI;
    private UserAPI userAPI;

    public ShopAPI() {
        paymentAPI = new PaymentAPI();
        productAPI = new ProductAPI();
        userAPI = new UserAPI();
    }

    void register(String login, String password) {
        userAPI.register(login, password);
    }

    void login(String login, String password) {
        userAPI.login(login, password);
    }

    void logout() {
        userAPI.logout();
    }

    void buyProduct(String productName) {
        paymentAPI.pay(productName, productAPI, userAPI);
    }

    void addProduct(String productName, float price) {
        productAPI.addProduct(productName, price);
    }

    void removeProduct(String productName) {
        productAPI.removeProduct(productName);
    }

    void feedWallet(float amount){
        userAPI.feedWallet(amount);
    }
}
