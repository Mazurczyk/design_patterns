package fasada;

import java.util.ArrayList;
import java.util.List;

public class UserAPI {

    private List<User> userList;


    public UserAPI() {
        userList = new ArrayList<>();
    }

    public void register(String login, String password) {
        userList.add(new User(login, password));
    }

    public void login(String login, String password) {
        for (User u : userList) {
            if (u.getLogin().equals(login) && u.getPassword().equals(password)) u.setLogin(true);
        }
    }

    public void logout() {
        getActiveUser().setLogin(false);
    }

    public void feedWallet(float amount) {
        User user = getActiveUser();
        user.setWallet(user.getWallet() + amount);
    }

    public User getActiveUser() {
        for (User u : userList) {
            if (u.isLogin() == true) return u;
        }
        return null;
    }
}
