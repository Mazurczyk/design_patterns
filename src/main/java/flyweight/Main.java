package flyweight;

//1. Napisz mechanizm renderowania/generowania drzew, krzewów w grze komputerowej. drzewa 3 roślin:
//- drzewa iglaste
//- drzewa liściaste
//- bukszpan
//Zadbaj o wydajność pamięciową działania aplikacji.

public class Main {

    public static void main(String[] args) {

        PlantFactory plantFactory = new PlantFactory();
        Plant boxwood = plantFactory.getPlant("boxwood");
        Plant leafyTree = plantFactory.getPlant("leafyTree");
        Plant conifer = plantFactory.getPlant("conifer");
        Plant boxwood1 = plantFactory.getPlant("boxwood");
        Plant leafyTree1 = plantFactory.getPlant("leafyTree");
        Plant conifer1 = plantFactory.getPlant("conifer");

        boxwood.draw();
        leafyTree.draw();
        conifer.draw();
        boxwood1.draw();
        leafyTree1.draw();
        conifer1.draw();
    }

}
