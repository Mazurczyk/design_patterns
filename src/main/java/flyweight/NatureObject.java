package flyweight;

public interface NatureObject {
    void draw();
}
