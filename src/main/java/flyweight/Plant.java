package flyweight;

public class Plant implements NatureObject {

    String type;

    public Plant(String type) {
        this.type = type;
    }

    @Override
    public void draw() {
        System.out.println("This plant " + this + " is " + type);
    }
}
