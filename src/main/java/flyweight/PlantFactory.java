package flyweight;

import java.util.HashMap;
import java.util.Map;

public class PlantFactory {

    Map<String, Plant> plantMap = new HashMap<>();

    public Plant getPlant(String type) {
        Plant plant = plantMap.get(type);
        if (plant == null) {
            plant = new Plant(type);
            plantMap.put(type, plant);
        }
        return plant;
    }
}
