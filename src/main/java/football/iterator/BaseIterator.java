package football.iterator;

public interface BaseIterator<T extends Player> {
    boolean hasNext();
    T next();
}
