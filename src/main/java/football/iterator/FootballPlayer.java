package football.iterator;

public class FootballPlayer extends Player {

    protected Position position;
    protected int skill;

    public FootballPlayer(String name, String lastName, Position position, int skill) {
        super(name, lastName);
        this.position = position;
        this.skill = skill;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "position=" + position +
                ", skill=" + skill +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
