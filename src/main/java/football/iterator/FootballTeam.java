package football.iterator;

public class FootballTeam extends Team<FootballPlayer> {

    @Override
    void add(FootballPlayer player) {
        playerList.add(player);
    }

    @Override
    public BaseIterator<FootballPlayer> getIterator() {
        return new FootballIterator();
    }

    private class FootballIterator implements BaseIterator<FootballPlayer> {

        private int position;

        @Override
        public boolean hasNext() {
            if (position < playerList.size()) return true;
            else return false;
        }

        @Override
        public FootballPlayer next() {
            if (hasNext()) return playerList.get(position++);
            else return null;
        }
    }
}
