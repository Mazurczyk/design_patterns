package football.iterator;

public class Main {
    public static void main(String[] args) {
        Player player1 = new FootballPlayer("Rafał", "Mazurczyk", Position.STRICKER, 99);
        Player player2 = new FootballPlayer("Diego", "Maradona", Position.MIDFIELDER, 100);
        Player player3 = new FootballPlayer("Kamil", "Glik", Position.DEFENDER, 80);
        Player player4 = new FootballPlayer("Artur", "Boruc", Position.GOALKIPPER, 70);
        Team<FootballPlayer> footballTeam = new FootballTeam();
        footballTeam.add((FootballPlayer) player1);
        footballTeam.add((FootballPlayer) player2);
        footballTeam.add((FootballPlayer) player3);
        footballTeam.add((FootballPlayer) player4);
        BaseIterator<FootballPlayer> iterator = footballTeam.getIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
