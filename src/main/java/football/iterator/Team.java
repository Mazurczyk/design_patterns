package football.iterator;

import java.util.ArrayList;
import java.util.List;

public abstract class Team<T extends Player> {

    protected List<T> playerList;


    public Team() {
        this.playerList = new ArrayList<>();
    }

    abstract void add(T player);

    abstract BaseIterator<T> getIterator();

}
