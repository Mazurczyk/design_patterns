package mediator;

public interface Dispatcher {
    void newOrder(String order);
    void addDriver(Driver driver);
}
