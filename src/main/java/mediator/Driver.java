package mediator;

public abstract class Driver {
    private String name;
    private Dispatcher dispatcher;

    public Driver(String name, Dispatcher dispatcher) {
        this.name = name;
        this.dispatcher = dispatcher;
    }

    abstract void onNewOrder(String orderName, Long orderID);

    abstract void endOrder();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }
}
