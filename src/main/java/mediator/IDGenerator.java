package mediator;

import java.util.Random;

public class IDGenerator {

    Random random = new Random();

    public Long next() {
        return Math.abs(random.nextLong());
    }
}
