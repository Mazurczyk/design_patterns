package mediator;

public class TaxiCompany {
    public static void main(String[] args) {
        Dispatcher dispatcher = new TaxiDispatcher();
        Driver driver1 = new TaxiDriver("Mietek", dispatcher);
        Driver driver2 = new TaxiDriver("Zbychu", dispatcher);
        Driver driver3 = new TaxiDriver("Zenek", dispatcher);
        dispatcher.addDriver(driver1);
        dispatcher.addDriver(driver2);
        dispatcher.addDriver(driver3);
        dispatcher.newOrder("Taxi on address: Witkiewicza 19, Szczecin");
        dispatcher.newOrder("Taxi on address: Witkiewicza 19, Szczecin");
        dispatcher.newOrder("Taxi on address: Witkiewicza 19, Szczecin");
        driver1.endOrder();
        dispatcher.newOrder("Taxi on address: Witkiewicza 19, Szczecin");
    }
}
