package mediator;

import java.util.LinkedList;
import java.util.Queue;

public class TaxiDispatcher implements Dispatcher {

    private Queue<Driver> drivers = new LinkedList<>();
    private IDGenerator idGenerator = new IDGenerator();

    @Override
    public void newOrder(String order) {
        if (!drivers.isEmpty()) {
            drivers.poll().onNewOrder(order, idGenerator.next());
        } else System.out.println("All drivers are busy. Please wait!");
    }

    @Override
    public void addDriver(Driver driver) {
        drivers.add(driver);
    }
}
