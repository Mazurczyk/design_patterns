package mediator;

public class TaxiDriver extends Driver {

    public TaxiDriver(String name, Dispatcher dispatcher) {
        super(name, dispatcher);
    }

    @Override
    void onNewOrder(String orderName, Long orderID) {
        System.out.println("------------NEW ORDER-------------");
        System.out.println("Order has been received by " + super.getName());
        System.out.println("Order ID: " + orderID);
        System.out.println(orderName);
    }

    @Override
    void endOrder() {
        super.getDispatcher().addDriver(this);
    }


}
