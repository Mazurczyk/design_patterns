package metoda.wytworcza;

public abstract class Button {
    protected ButtonType buttonType;

    public Button(ButtonType buttonType) {
        this.buttonType = buttonType;
    }

    abstract void onClick();

}
