package metoda.wytworcza;

public interface ButtonFactory{

    Button getButton(ButtonType type);

}
