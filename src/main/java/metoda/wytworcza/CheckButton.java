package metoda.wytworcza;

public class CheckButton extends Button{

    public CheckButton() {
        super(ButtonType.CHECK);
    }

    @Override
    void onClick() {
        System.out.println("Check button has been clicked.");
    }
}
