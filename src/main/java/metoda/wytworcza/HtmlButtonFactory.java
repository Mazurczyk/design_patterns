package metoda.wytworcza;

public class HtmlButtonFactory implements ButtonFactory {
    @Override
    public Button getButton(ButtonType type) {
        switch (type) {
            case SUBMIT:
                return new SubmitButton();
            case RADIO:
                return new RadioButton();
            case CHECK:
                return new CheckButton();
            default:
                return null;
        }
    }
}
