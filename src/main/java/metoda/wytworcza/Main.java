package metoda.wytworcza;

public class Main {
    public static void main(String[] args){
        ButtonFactory factory = new HtmlButtonFactory();
        Button button1 = factory.getButton(ButtonType.SUBMIT);
        Button button2 = factory.getButton(ButtonType.RADIO);
        Button button3 = factory.getButton(ButtonType.CHECK);
        button1.onClick();
        button2.onClick();
        button3.onClick();
    }
}
