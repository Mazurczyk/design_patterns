package metoda.wytworcza;

public class RadioButton extends Button {
    public RadioButton() {
        super(ButtonType.RADIO);
    }

    @Override
    void onClick() {
        System.out.println("Radio button has been clicked.");
    }
}
