package metoda.wytworcza;

public class SubmitButton extends Button{

    public SubmitButton() {
        super(ButtonType.SUBMIT);
    }

    @Override
    void onClick() {
        System.out.println("Submit button has been clicked.");
    }
}
