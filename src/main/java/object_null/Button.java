package object_null;

public class Button implements UIComponent {

    String buttonType = "button";

    @Override
    public void onClick() {
        System.out.println("On button click.");
    }
}
