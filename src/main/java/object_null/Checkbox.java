package object_null;

public class Checkbox implements UIComponent {

    String buttonType = "checkbox";

    @Override
    public void onClick() {
        System.out.println("On checkbox click.");
    }
}
