package object_null;

//Napisz własną implementację obiektu Null dla edytora graficzne w tym celu zaimplementuj:
//- interface UIComponent z metodą onClick
//- klasę Button implementującą UIComponent
//- klasę CheckBox implementującą UIComponent
//- obiekt null
//- klasę UIComponentFactory która w przypadku braku kompatybilnego komponentu zwróci nam obiekt null

public class Main {
    public static void main(String[] args) {

        UIComponentFactory uiComponentFactory = new UIComponentFactory();
        UIComponent uiComponent = uiComponentFactory.getUIComponent("");
        uiComponent.onClick();

    }
}
