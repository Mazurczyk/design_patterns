package object_null;

public class NullObject implements UIComponent{
    @Override
    public void onClick() {
        System.out.println("On null object click!");
    }
}