package object_null;

public class UIComponentFactory {
    public UIComponent getUIComponent(String UIComponentType) {
        if ("Button".equalsIgnoreCase(UIComponentType)) {
            return new Button();
        } else if ("Checkbox".equalsIgnoreCase(UIComponentType)) {
            return new Checkbox();
        } else return new NullObject();
    }
}
