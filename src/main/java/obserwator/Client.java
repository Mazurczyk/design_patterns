package obserwator;

public interface Client {
    void notifyAboutNews(String shopName, News news);
}
