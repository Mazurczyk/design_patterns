package obserwator;

public class NewsPublisher {
    public static void main(String[] args) {
        News news = new News("We have the new promotional leaflet!");
        Shop shop = new WebShop("Biedronka");
        Client client = new WebClient();
        shop.subscribe(client);
        shop.addNews(news);
        Client client2 = new OnlineClient();
        shop.subscribe(client2);
        News news2 = new News("We have special offer for new clients!");
        shop.addNews(news2);
    }
}
