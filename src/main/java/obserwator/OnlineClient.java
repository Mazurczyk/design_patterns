package obserwator;

public class OnlineClient implements Client {
    @Override
    public void notifyAboutNews(String shopName, News news) {
        System.out.println("News from " + shopName + " to online client: " + news.getContent());
    }
}
