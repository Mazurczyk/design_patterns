package obserwator;

public interface Shop {
    void subscribe(Client client);
    void unsubscribe(Client client);
    void addNews(News news);
}
