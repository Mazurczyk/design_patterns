package obserwator;

public class WebClient implements Client {
    @Override
    public void notifyAboutNews(String shopName, News news) {
        System.out.println("News from " + shopName + " to web client: " + news.getContent());
    }
}
