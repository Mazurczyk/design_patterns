package obserwator;

import java.util.ArrayList;
import java.util.List;

public class WebShop implements Shop {

    private List<Client> clients;
    private List<News> news;
    private String name;

    public WebShop(String name) {
        this.name = name;
        clients = new ArrayList<>();
        news = new ArrayList<>();
    }

    @Override
    public void subscribe(Client client) {
        if (!clients.contains(client)) {
            clients.add(client);
        }
    }

    @Override
    public void unsubscribe(Client client) {
        if (clients.contains(client)) {
            clients.remove(client);
        }
    }

    @Override
    public void addNews(News news) {
        this.news.add(news);
        announceLastNews(news);
    }

    public void announceLastNews(News news) {
        for (Client client : clients) {
            client.notifyAboutNews(name, news);
        }
    }
}
