package pizza.dekorator;

public class Ham extends PizzaDecorator {
    public Ham(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void makePizza() {
        super.makePizza();
        System.out.println("Ham has been added!");
    }
}
