package pizza.dekorator;

public class Jalapenio extends PizzaDecorator {
    public Jalapenio(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void makePizza() {
        super.makePizza();
        System.out.println("Jalapenio has been added!");
    }
}
