package pizza.dekorator;

public class Main {

    public static void main(String[] args) {
        Pizza margaritta = new Margaritta();
        System.out.println("------------Base pizza---------------");
        margaritta.makePizza();
        Pizza pepperoni = new PizzaDecorator(new Pepperoni(margaritta));
        System.out.println("------------Pizza pepperoni---------------");
        pepperoni.makePizza();
        Pizza ham = new PizzaDecorator(new Ham(margaritta));
        System.out.println("------------Pizza with ham---------------");
        ham.makePizza();
        Pizza pepperoniWithHam = new Pepperoni(ham);
        System.out.println("------------Pizza pepperoni with ham---------------");
        pepperoniWithHam.makePizza();
        Pizza pepperoniWithHamAndJalapenio = new Jalapenio(pepperoniWithHam);
        System.out.println("------------Pizza pepperoni with ham and jalapenio---------------");
        pepperoniWithHamAndJalapenio.makePizza();
    }
}
