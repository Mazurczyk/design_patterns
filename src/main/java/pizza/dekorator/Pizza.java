package pizza.dekorator;

public interface Pizza {
    void makePizza();
}
