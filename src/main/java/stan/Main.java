package stan;

public class Main {
    public static void main(String[] args) {
        Phone phone = new Phone();
        phone.showVolumeLevel();
        phone.volumeUp();
        for (int i = 0; i < 100; i++) {
            phone.volumeDown();
            phone.showVolumeLevel();
        }
        phone.volumeDown();
        phone.showVolumeLevel();
        phone.volumeDown();
    }
}
