package stan;

public class Mute implements VolumeState {
    @Override
    public void volumeUp(Phone phone) {
        phone.setVolumeState(new Vibration());
        phone.setVolumeLevel(0);
    }

    @Override
    public void volumeDown(Phone phone) {
        System.out.println("There is no previous stan!");
    }

    @Override
    public void showStatus(Phone phone) {
        System.out.println("Phone state is " + phone.getVolumeState().getClass().getName().substring(5));
    }
}
