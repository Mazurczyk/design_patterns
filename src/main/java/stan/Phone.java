package stan;

public class Phone {
    private VolumeState volumeState;
    private int volumeLevel;

    public Phone() {
        volumeState = new Sound();
        volumeLevel = 100;
    }

    public void volumeUp() {
        volumeState.volumeUp(this);
    }

    public void volumeDown() {
        volumeState.volumeDown(this);
    }

    public void showVolumeLevel() {
        volumeState.showStatus(this);
    }

    public VolumeState getVolumeState() {
        return volumeState;
    }

    public void setVolumeState(VolumeState volumeState) {
        this.volumeState = volumeState;
    }

    public int getVolumeLevel() {
        return volumeLevel;
    }

    public void setVolumeLevel(int volumeLevel) {
        this.volumeLevel = volumeLevel;
    }
}
