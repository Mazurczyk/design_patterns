package stan;

public class Sound implements VolumeState {
    @Override
    public void volumeUp(Phone phone) {
        if (phone.getVolumeLevel() < 100) {
            phone.setVolumeLevel(phone.getVolumeLevel() + 1);
        } else {
            System.out.println("There is max volume!");
        }
    }

    @Override
    public void volumeDown(Phone phone) {
        if (phone.getVolumeLevel() == 1) {
            phone.setVolumeState(new Vibration());
        }
        phone.setVolumeLevel(phone.getVolumeLevel() - 1);
    }

    @Override
    public void showStatus(Phone phone) {
        System.out.println("Phone state is " + phone.getVolumeState().getClass().getName().substring(5) + " on level " +
                phone.getVolumeLevel());
    }
}
