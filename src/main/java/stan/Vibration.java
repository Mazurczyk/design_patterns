package stan;

public class Vibration implements VolumeState {
    @Override
    public void volumeUp(Phone phone) {
        phone.setVolumeState(new Sound());
        phone.setVolumeLevel(1);
    }

    @Override
    public void volumeDown(Phone phone) {
        phone.setVolumeState(new Mute());
        phone.setVolumeLevel(-1);
    }

    @Override
    public void showStatus(Phone phone) {
        System.out.println("Phone state is " + phone.getVolumeState().getClass().getName().substring(5));
    }
}
