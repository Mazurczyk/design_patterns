package stan;

public interface VolumeState {
    void volumeUp(Phone phone);
    void volumeDown(Phone phone);
    void showStatus(Phone phone);
}
