package flyweight;

import flyweight.Plant;
import flyweight.PlantFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlantFactoryTest {

    private PlantFactory plantFactory;

    @BeforeEach
    public void init(){
        plantFactory = new PlantFactory();
    }

    @Test
    public void test_if_the_same_bird_is_returned(){
        //given
//        PlantFactory plantFactory= new PlantFactory();
        //when
        Plant plant = plantFactory.getPlant("tree");
        Plant plant1 = plantFactory.getPlant("tree");
        //then
        assertSame(plant,plant1);
    }

}