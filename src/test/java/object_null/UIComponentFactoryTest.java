package object_null;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UIComponentFactoryTest {

    private UIComponentFactory uiComponentFactory;

    @BeforeEach
    public void init() {
        uiComponentFactory = new UIComponentFactory();
    }

    @Test
    public void test_if_null_object_is_returned() {
        //given
        String UIComponentType = "unknown";
        //when
        UIComponent uiComponent = uiComponentFactory.getUIComponent(UIComponentType);
        //then
        assertNotNull(uiComponent);
        assertSame(NullObject.class, uiComponent.getClass());
    }

    @Test
    public void test_if_button_object_is_returned() {
        //given
        String UIComponentType = "button";
        //when
        UIComponent uiComponent = uiComponentFactory.getUIComponent(UIComponentType);
        //then
        assertNotNull(uiComponent);
        assertSame(Button.class, uiComponent.getClass());
    }

}